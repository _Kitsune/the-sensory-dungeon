using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSlider : MonoBehaviour
{
    ddol_Data data;     //Dane u�ytkownika
    Slider slider;      //Slider do kt�rego si� odnosimy

    void Start()
    {
        //bierzemy dane, je�li nie istniej� wyrzucamy b��d
        data = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>();
        if(data==null)
        {
            throw new Exception("Can not located dont destroy on load data object");
        }

        slider = GetComponent<Slider>();
        slider.value = data.SoundVolume; //ustawiamy warto�� slidera na t� ktor� mieli�my w danych u�ytkownika
    }


    void Update()
    {
        data.SoundVolume = (int)slider.value;   //Zmieniamy dane by zgadza�y si� z warto�ci� slidera
    }
}
