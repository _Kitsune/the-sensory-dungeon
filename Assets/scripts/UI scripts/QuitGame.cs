using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Skrypt z funkcj� do wychodzenia z gry
/// </summary>
public class QuitGame : MonoBehaviour
{
    /// <summary>
    /// Wychodzi z gry
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}
