using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Skrypt do zmieniania sceny 
/// </summary>
public class SceneChange : MonoBehaviour
{
    /// <summary>
    /// Zmienia scene na podan� nazw� sceny
    /// </summary>
    /// <param name="name">Nazwa nowej sceny</param>
    public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }
    
}
