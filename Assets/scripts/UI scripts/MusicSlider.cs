using System;
using UnityEngine;
using UnityEngine.UI;

public class MusicSlider : MonoBehaviour
{
    ddol_Data data;     //Dane u�ytkownika
    Slider slider;      //Slider do kt�rego si� odnosimy
    void Start()
    {
        //Sprawdzamy czy mo�emy si� dosta� do obiektu z Dont Destroy on Load
        data = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>();
        if (data == null)
        {
            //je�li nie wyrzucamy b��d
            throw new Exception("Can not located dont destroy on load data object");
        }
        slider = GetComponent<Slider>();
        //Ustawiamy warto�� slidera na t� kt�ra jest w danych
        slider.value = data.MusicVolume;
    }


    void Update()
    {
        //przechowujemy warto�� slidera w skrypcie z danymi
        data.MusicVolume = (int)slider.value;
    }
}
