using UnityEngine;
using TMPro;
using UnityEngine.Localization;

public class UserDataShower : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI idText;        //Obiekt w kt�rym wy�wietlimy id
    [SerializeField] TextMeshProUGUI keyText;       //Obiekt w kt�rym wy�wietlimy klucz
    [SerializeField] TextMeshProUGUI nameText;       //Obiekt w kt�rym wy�wietlimy klucz

    [SerializeField] LocalizedString idString;      //String do kt�ry powie co mamy wypisa�, ju� w dobrym j�zyku
    [SerializeField] LocalizedString keyString;     //String do kt�ry powie co mamy wypisa�, ju� w dobrym j�zyku
    [SerializeField] LocalizedString nameString;     //String do kt�ry powie co mamy wypisa�, ju� w dobrym j�zyku

    ddol_Data data;                                 //Dane u�ytkownika

    /// <summary>
    /// Funkcja do zdarzenia zmiany j�zyka dla klucza
    /// </summary>
    /// <param name="newString"></param>
    void UpdateKey(string newString)
    {
        keyText.SetText(newString + data.UserKey);
    }

    /// <summary>
    /// Funkcja do zdarzenia zmiany j�zyka dla id
    /// </summary>
    void UpdateId(string newString)
    {
        idText.SetText(newString + " " + data.UserId);
    }
    void UpdateName(string newString)
    {
        nameText.SetText(newString + GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>().CurrentUser.userName);
    }


    /// <summary>
    /// Funkcja do pierwszego ustawienia danych w stringach
    /// </summary>
    void UpdateData()
    {
        idText.SetText(idString.Values + " " +data.UserId.ToString());
        keyText.SetText(keyString.Values + " " + data.UserKey.ToString());
        nameText.SetText(nameString.Values + " " + GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>().CurrentUser.userName);
    }

    /// <summary>
    /// Funkcja do za�adowania danych z DDOL
    /// W przypadku nie znalezienia obiektu stworzy w�asny lokalny
    /// </summary>
    /// oIo
    /// ( .)(. )
    void LoadData()
    {
        if (GameObject.FindGameObjectsWithTag("DDOL").Length==0)
        {
            data = new ddol_Data();
            return;
        }
        data = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>();
    }


    private void Start()
    {
        LoadData();
        UpdateData();
    }
    private void Update()
    {
        keyString.RefreshString();
        idString.RefreshString();
        nameString.RefreshString();
    }
    private void OnEnable()
    {
        LoadData();

        idString.Arguments = new[] { this };
        idString.StringChanged += UpdateId;
        keyString.Arguments = new[] { this };
        keyString.StringChanged += UpdateKey;
        nameString.Arguments = new[] { this };
        nameString.StringChanged += UpdateName;
    }

}
