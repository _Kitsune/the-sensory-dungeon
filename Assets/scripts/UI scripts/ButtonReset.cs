using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Skrypt do dodania do guzika aby przy nast�pnym wej�ciu do sceny z losowym u�o�eniem pokoi je wygenerowa�
/// </summary>
public class ButtonReset : MonoBehaviour
{

    /// <summary>
    /// Dodaje do guzika event na klikni�cie aby resetowa� wygenrowane pokoje
    /// </summary>
    /// <param name="value"></param>
    public void SetToGenerate(bool value)
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(delegate
        {
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().GenerateRooms = value;
        });
    }

    private void Start()
    {
        SetToGenerate(true);
    }
}
