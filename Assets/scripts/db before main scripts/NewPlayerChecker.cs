using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerChecker : MonoBehaviour
{
    void Start()
    {
        
        if(PlayerPrefs.HasKey("userName"))          //where we logged in before
        {
            //Get data from db and player prefs
            dbmanager temp_db = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
            temp_db.CurrentUser = new dbmanager.User();   
            temp_db.Key = PlayerPrefs.GetString("userKey");

            if (temp_db.Finished == 1)                 //Correctly selected data
            {
                temp_db.CurrentUser.userName = PlayerPrefs.GetString("userName");
                temp_db.CurrentUser.id = PlayerPrefs.GetInt("userId");
                GetComponent<SceneChange>().ChangeScene("MainMenu");

            }
            else if (temp_db.Finished == -2)        //We need to select the data
            {
                temp_db.Select(PlayerPrefs.GetString("userName"));
            }

        }
        else                //First time login, go to create
        {
            GetComponent<SceneChange>().ChangeScene("NewGame");
        }

        if(GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline)  //We are offline
        {
            GetComponent<SceneChange>().ChangeScene("MainMenu");
        }
    }

 
}
