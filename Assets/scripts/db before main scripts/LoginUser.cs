using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoginUser : MonoBehaviour
{
    [SerializeField] LocalizedStringHandler wrongAnswer;        //output for incorrect login
    [SerializeField] TextMeshProUGUI username;                  //input text
    [SerializeField] TextMeshProUGUI key;                       //input text
    [SerializeField] TextMeshProUGUI output;                    //output text
    dbmanager database;
    bool ran = false;                                           //already tried to login - for now clicking a few times when it connects to the server

    private void Start()
    {
        wrongAnswer.Init();
    }
    public void onClick()
    {
        ran = true;
        database = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
        StartCoroutine(database.TryToLogin(username.text, key.text));
    }
    private void Update()
    {
        if (!ran)
            return;

        if(database.Logged==1)  //we logged
        {
            database.Logged = 0;
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<SceneChange>().ChangeScene("MainMenu");
        }else if (database.Logged == -1)        //we aint
        {
            database.Logged = 0;
            output.SetText(wrongAnswer.UsageString);
        }
        
    }

}
