using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    [SerializeField] GameObject Interface;  //Pause window  
    /// <summary>
    /// Pauses the game
    /// </summary>
    public void Pause()
    {
        if(Time.timeScale>0)
        {
            Time.timeScale = 0;
            Interface.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            Interface.SetActive(false);
        }
        

    }
}
