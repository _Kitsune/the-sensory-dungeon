using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static GenerateDungeonRooms;
using UnityEngine.Localization;
using System.Security;

public class AccuracyTestManager : MonoBehaviour
{
    [SerializeField] Image panelImage;                           //To change alpha when start
    [SerializeField] Image borderimage;                           //To change alpha when start

    [SerializeField] LocalizedStringHandler timeRunOutString;   //Tekst kt�ry si� wy�wietli m�wi�cy o tym �e czas si� sko�czy�
    [SerializeField] LocalizedStringHandler secondsString;      //Sekundy w dobrym j�zyku
    [SerializeField] LocalizedStringHandler scoreString;         //Tekst do wyniku

    [SerializeField]TextMeshProUGUI timerObject;    //Obiekt w kt�rym wy�wietlamy pozosta�y czas
    [SerializeField]TextMeshProUGUI lastTimerObject;//Obiekt w kt�rym wy�wietlamy �rednia
    [SerializeField]TextMeshProUGUI scoreObject;    //Obiekt w kt�rym wy�wietlamy zdobyte punkty
    [SerializeField]GameObject panel;               //panel w kt�rym genereujemy guziki
    [SerializeField]GameObject button;              //baza guzika kt�ry generujemy

    [SerializeField] GameObject finishScreen;
    [SerializeField] TextMeshProUGUI endScoreText;


    public int count = 0;                                  //ilo�� klikni��
    [SerializeField] float maxTime = 20;            // d�ugo�� test w sekundach
    float timeLeft = 0;                             //ile czasu testu pozosta�o

    Vector2 panelSize;                              //Rozmiar panelu w kt�rym ma si� pojawi�
    Vector2 panelPosition;                          //Pozycja panelu w kt�rym ma si� pojawi�
    Vector2 buttonSize;                             //Rozmiar guzika, tego kt�ry si� pojawi


    bool running = false;                           //Czy test chodzi
    public bool ForeignManager = false;             //Czy sterujemy tym testem ze skryptu z zewn�trz

    GameObject currentButton;                       //Guzik kt�ry si� aktualnie wy�wietla

    /// <summary>
    /// Generuje guzik do klikni�cie (button) w losowej pozycji w panelu (panel)
    /// </summary>
    public void GenerateButton()
    {
        //poniewa� po skalowaniu mog� wyj�� ujemne warto�ci, bierzemy warto�� bezwzgl�dn�
        panelSize.x = Mathf.Abs(panelSize.x);
        panelSize.y = Mathf.Abs(panelSize.y);

        //Tworzymy pozycje obiektu w panelu
        //Pozycja panelu zaczyna si� od jego �rodka, tak jak i jego rozmiar
        //Zaczynamy od pozycji zero zero minus rozmiar i dodatego dobrym dzia�aniem dajemy 3 rozmiary guzika (3 * 1/2 guzika), �eby nie wychodzi� i nie by� na linii
        Vector3 ObjectPosition = new Vector3(UnityEngine.Random.Range(panelPosition.x - panelSize.x + (buttonSize.x*3), panelPosition.x + panelSize.x - (buttonSize.x*3)),
        UnityEngine.Random.Range(panelPosition.y - panelSize.y + (buttonSize.y *3), panelPosition.y + panelSize.y - (buttonSize.y*3)),
        0);

        //Tworzymy obiekt
        currentButton = Instantiate(button, ObjectPosition/50, Quaternion.identity);
        //nadajemy mu rodzica
        currentButton.transform.SetParent(panel.transform, false);
        //zmieniamy lokaln� pozycje
        currentButton.transform.localPosition = ObjectPosition;

        //Dodajemy mu onClick by test m�g� dzia�a�
        currentButton.GetComponent<Button>().onClick.AddListener(Clicked);
    }

    public void GenerateButton(float sizeX, float sizeY)
    {
        //poniewa� po skalowaniu mog� wyj�� ujemne warto�ci, bierzemy warto�� bezwzgl�dn�
        panelSize.x = sizeX;
        panelSize.y = sizeY;

        //Tworzymy pozycje obiektu w panelu
        //Pozycja panelu zaczyna si� od jego �rodka, tak jak i jego rozmiar
        //Zaczynamy od pozycji zero zero minus rozmiar i dodatego dobrym dzia�aniem dajemy 3 rozmiary guzika (3 * 1/2 guzika), �eby nie wychodzi� i nie by� na linii
        Vector3 ObjectPosition = new Vector3(UnityEngine.Random.Range(panelPosition.x - panelSize.x + (buttonSize.x * 3), panelPosition.x + panelSize.x - (buttonSize.x * 3)),
        UnityEngine.Random.Range(panelPosition.y - panelSize.y + (buttonSize.y * 3), panelPosition.y + panelSize.y - (buttonSize.y * 3)),
        0);

        //Tworzymy obiekt
        currentButton = Instantiate(button, ObjectPosition / 50, Quaternion.identity);
        //nadajemy mu rodzica
        currentButton.transform.SetParent(panel.transform, false);
        //zmieniamy lokaln� pozycje
        currentButton.transform.localPosition = ObjectPosition;

        //Dodajemy mu onClick by test m�g� dzia�a�
        currentButton.GetComponent<Button>().onClick.AddListener(Clicked);
    }

    /// <summary>
    /// Pobiera konieczne informacje z obiekt�w, odpala test(Tworzy pierwszy obiekt i nadaje czas)
    /// Powinien by� odpalony przez onClick na guziku
    /// </summary>
    public void StartTest()
    {
        panelSize = panel.GetComponent<RectTransform>().sizeDelta;
        panelPosition = panel.GetComponent<RectTransform>().position;
        buttonSize = button.GetComponent<RectTransform>().sizeDelta;

        timeLeft = maxTime;
        running = true;

        panelImage.color = new Color(panelImage.color.r, panelImage.color.g, panelImage.color.b,1);
        borderimage.color = new Color(borderimage.color.r, borderimage.color.g, borderimage.color.b, 1); ;
        GenerateButton();
    }
    /// <summary>
    /// zwi�ksza licznik punkt�w, usuwa guzik
    /// Powinien si� odpali� w momencie klikni�ci guzika button
    /// </summary>
    public void Clicked()
    {
        count++;
        if(!ForeignManager)
            scoreObject.GetComponent<TextMeshProUGUI>().SetText(count.ToString());
        GameObject.Destroy(currentButton);
        GenerateButton();
    }
   
    /// <summary>
    /// Zmienia tekst obiektu stopera i usuwa obecny guzik
    /// 
    /// </summary>
    public void FinishTest()
    {
        running = false;
        if(!ForeignManager)
        {
            lastTimerObject.GetComponent<TextMeshProUGUI>().SetText(timeRunOutString.UsageString);
            timerObject.SetText("");
        }
           

        GameObject.Destroy(currentButton);
        



        int additionalPoints = count;
        finishScreen.SetActive(true);

        if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
        {
            endScoreText.SetText(scoreString.UsageString + additionalPoints);
            gameObject.SetActive(false);
            return;
        }

        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount > 0)
        {
            additionalPoints = (int)(additionalPoints / 2);
        }

        endScoreText.SetText(scoreString.UsageString + additionalPoints);
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

        dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
        StartCoroutine(temp_manager.UpdatePoints(additionalPoints));

        GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();

        gameObject.SetActive(false);

    }
   
    /// <summary>
    /// Test sprawdza pozycje, czy znajduje si� w panelu
    /// Poprawny dla rozdzielczo�ci 16 x 9 
    /// </summary>
    void Test_GenerateButton()
    {
        StartTest();
        running = false;
        for (int i = 0; i < 1000; i++)
        {
            GenerateButton();
        }
    }

    private void Start()
    {
        //inicjowanie tekstu
        timeRunOutString.Init();    
        secondsString.Init();
    }
    private void Update()
    {
        if (!running)
            return;


        //zmniejsz czas
        timeLeft -= Time.deltaTime;

        //Ko�czy test i wychodzi by nie nadpisa� tekstu w timerObject
        if(timeLeft<=0)
        {
            FinishTest();
            return;
        }

        //pokazuje pozosta�y czas
        timerObject.GetComponent<TextMeshProUGUI>().SetText(System.Math.Round(timeLeft,3).ToString() + " " + secondsString.UsageString);
    }
}
