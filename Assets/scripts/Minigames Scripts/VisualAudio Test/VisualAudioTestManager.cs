using System.Collections;
using System.Collections.Generic;
using System.Security;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VisualAudioTestManager : MonoBehaviour
{

    enum Type                                                       //Jaki typ testu b�dzie
    {
        visual,
        audio,
        both
    }


    [SerializeField] LocalizedStringHandler avarageClickString; //Tekst kt�ry si� poka�e przy podsumowaniu ze �redni�
    [SerializeField] LocalizedStringHandler scoreString;        //Tekst do wyniku
    [SerializeField] AudioClip toClickSound;                    //D�wi�k po kt�rym nale�y klikn��

    [SerializeField] Color baseColor = Color.green;             //Bazowy kolor guzika
    [SerializeField] Color activeColor = Color.red;             //Kolor guzika kiedy mamy go klikn��


    [SerializeField]public int TestedInputs = 10;               //Ile razy u�ytkownik ma klika� w te�cie

    [SerializeField] TextMeshProUGUI text;                      //Obiekt w kt�rym wy�wietlimy czas
    [SerializeField] TextMeshProUGUI scoreText;                 //Obiekt wynik
    [SerializeField] TextMeshProUGUI finalTimeText;             //Obiekt w kt�ym wy�wietlimy ko�cowy wynik
    [SerializeField] Image colorEffector;                       //Obiekt kt�rego kolor zmieniamy podczas testu wizualnego
    [SerializeField] AudioSource audioPlayer;                   //Komponenty na kt�rym odpalamy klip

    [SerializeField] GameObject finishScreen;                   //Object with all of the elements for finishing - panel with score
    [SerializeField] TextMeshProUGUI endScoreText;              //Text in which we will show the final score

    bool running = false;                                       //Czy skrypt chodzi
    bool finish = false;                                        //Czy test si� zako�czy�
    bool waiting = false;                                       //Czy czekamy na wydarzenie kt�re sprawi �e u�ytkownik ma klikn�c

    bool CanClick = false;                                      //Czy u�ytkownik mo�e teraz klika� - mierzymy jego czas

    float timeToWait = 0;                                       //Ile czasu jest pomi�dzy testami, zmienia si� idzie do zera
    float maxWaitTime = 3;                                      //Ile trzeba czeka� maksymalnie mi�dzy klikni�ciami
    float minWaitTime = 0.4f;                                   //Ile minimalnie trzeba czeka�

    [SerializeField]MeasureTime timer;                          //Timer kt�ry mierzy czas klikni��

    Type[] testTypes = new Type[10];                            //Z jakich typ�w i kiedy sk�ada si� test

    public int testIndex = 0;                                   //Ktory test obecnie mamy

    public bool ForeignManager = false;                         //Czy sterujemy testem z innego skyrptu

    FightVisualSimulation fvs;                                  //Skrypt od symulacji walki

    /// <summary>
    /// Pierwsze rozpocz�cie testu
    /// Ustawia typy i usuwa obiekt {startObject}
    /// </summary>
    void StartTest()
    {
        running = true;

        for (int i = 0; i < testTypes.Length; i++) //losujemy Typy w tym te�cie
        {
            testTypes[i] = (Type)(UnityEngine.Random.Range(0, (int)Type.both + 1));
        }

    }
   
    /// <summary>
    /// Sprawdza czy przeszli�my poziom, czyli czy wszystkie testy z {testTypes} zrobili�my
    /// </summary>
    void CheckForTheEnd()
    {
        if (testIndex >= TestedInputs)
        {
            finish = true;
            finalTimeText.SetText(avarageClickString.UsageString + (timer.TimesSum / TestedInputs).ToString() + " ms");
            text.SetText("");


            float avg = (timer.TimesSum / TestedInputs);
            int additionalPoints = (int)(100 / (avg / 100));
            finishScreen.SetActive(true);

            if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
            {
                endScoreText.SetText(scoreString.UsageString + additionalPoints);
                return;
            }

            if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount > 0)
            {
                additionalPoints = (int)(additionalPoints / 2);
            }

            endScoreText.SetText(scoreString.UsageString + additionalPoints);
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

            dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
            StartCoroutine(temp_manager.UpdatePoints(additionalPoints));

            GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();

        }
    }
    
    /// <summary>
    /// Ustawia czas testu w przedziale - {timeToWait}
    /// </summary>
    void ResetTimer()
    {
        timeToWait += UnityEngine.Random.Range(minWaitTime, maxWaitTime);
    }
    
    /// <summary>
    /// Zwi�kszamy poziom, nast�pny test
    /// Reset timera, reset image, testIndex++
    /// </summary>
    void IncrementLevel()
    {
        testIndex++;
        ResetTimer();
        colorEffector.color = baseColor;
    }
    
    /// <summary>
    /// Funkcja kt�ra odpala si� przy klikni�ciu
    /// Start 
    /// Nowy poziom
    /// Reset Timera
    /// </summary>
    public void OnClick()
    {
        if (finish) //sko�czy� dzia�anie
            return;

        if (!running && !ForeignManager ) //pierwsze odpalenie
        {
            fvs = GetComponent<FightVisualSimulation>();
            StartTest();
            return;
        }
        if (CanClick || ForeignManager)   //Mo�emy klikn��, bo ju� mierzy czas
        {
            IncrementLevel();
            scoreText.SetText(testIndex.ToString());
            timer.StopMeasure();
            return;
        }
        ResetTimer();   //klikn�li�my ale nie powinni�my - reset timera
    }
    
    /// <summary>
    /// Odpala wizualnie lub d�wi�kowo testy, to co m�wi czym s�
    /// </summary>
    public void TestStartEffect()
    {
        if (waiting)    //by wykona� to tylko raz
            return;

        timer.StartMeasure();
        fvs.Activate();

        if (ForeignManager)
        {
            colorEffector.color = activeColor;
            audioPlayer.Play();
            return;
        }


        switch (testTypes[testIndex])
        {
            case Type.visual:
                colorEffector.color = activeColor;
                break;

            case Type.audio:
                audioPlayer.Play();
                break;

            case Type.both:
                colorEffector.color = activeColor;
                audioPlayer.Play();
                break;
        }
    }
   
    
    private void Update()
    {
        if (!running || finish)
            return;

        if (timeToWait > 0) //czekamy na event kt�ry rozpocznie mierzenie czasu reakcji
        {
            timeToWait -= Time.deltaTime;
            CanClick = false;
            waiting = false;
        }
        else    //Mierzymy czas reakcji
        {
            CanClick = true;
            TestStartEffect();
            waiting = true;
        }


        text.SetText(timer.TimeElapsed.ToString() + " ms");

        CheckForTheEnd();   //Sprawdzamy czy sko�czyli�my test

    }
    
    
    private void Start()
    {
        avarageClickString.Init();
        audioPlayer.clip = toClickSound;
        timer = GetComponent<MeasureTime>();
    }
}
