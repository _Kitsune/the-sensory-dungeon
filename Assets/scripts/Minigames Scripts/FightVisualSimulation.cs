using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FightVisualSimulation : MonoBehaviour
{
    [SerializeField] GameObject sword;      //Sword Object
    [SerializeField] GameObject shield;     //Shield Object
    [SerializeField] float minAngle;        //At what angle our swords start
    [SerializeField] float maxAngle;        //Where it ends
    [SerializeField] float angleSpeed;      //how fst does it rotate

    [SerializeField] float movementSpeed;   //how fast does it move
    [SerializeField] float shieldMovementSpeed;
    [SerializeField] Vector2 shieldMovementDirection;
    
    [SerializeField] float maxMovingTime = 0.1f;    //For how long will do sword move

    bool movingSword = false;
    float movingTime = 0.1f;
    Vector3 firstPosition = Vector3.zero;
    Vector3 shieldFirstPosition;
    bool backed = false;

    /// <summary>
    /// Start mobing
    /// </summary>
    public void Activate()
    {
        movingSword = true;
        movingTime = maxMovingTime;
    }
    /// <summary>
    /// Restart
    /// </summary>
    public void DeActivate()
    {
        movingSword=false;
        sword.transform.eulerAngles = new Vector3(0,0,minAngle);
        sword.transform.position = firstPosition;
        shield.transform.position = shieldFirstPosition;
        backed = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        Vector3 pos = sword.transform.position;
        firstPosition = pos;
        shieldFirstPosition = shield.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(!movingSword)
        {
            return;
        }

        Vector3 rot = sword.transform.eulerAngles;
        float zRot = rot.z;

        //rotate till got
        if(rot.z < maxAngle)
        {
            sword.transform.Rotate(0, 0, angleSpeed * Time.deltaTime);
        }
        else
        {
            sword.transform.eulerAngles = new Vector3(rot.x, rot.y, maxAngle);
        }

        //Moe for some time
        if(movingTime>0)
        {
            movingTime -= Time.deltaTime;
            Vector3 pos = sword.transform.position;
            sword.transform.position = new Vector3(pos.x - (movementSpeed * Time.deltaTime), pos.y, rot.z);
        }else
        {
            if (backed)
            {
                return;
            }
                

            backed = true;
            Vector3 temp_shieldPos = shield.transform.position;

            temp_shieldPos.x -= (shieldMovementSpeed * shieldMovementDirection).x;
            temp_shieldPos.y -= (shieldMovementSpeed * shieldMovementDirection).y;

            shield.transform.position = temp_shieldPos;
        }

        

    }
}
