using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class FinalTestManager : MonoBehaviour
{
    /* TODO
     * init testoq
    zmie� wielko�� liter
    clean up po testach

    */
    [SerializeField]int InputCounts = 30;
    [SerializeField] GameObject timingButton;
    [SerializeField] GameObject accPanel;


    Type[] types;


    [SerializeField] float[] scores;

    [SerializeField] VisualChange visualChange;
    [SerializeField] MeasureTime measureTime;

    [SerializeField] AudioTestManager audioTestManager;

    [SerializeField] VisualAudioTestManager visualAudioTestManager; //To init

    [SerializeField] SpatilTestManager spatialTestManager; //To init

    [SerializeField] SequenceTestManager sequenceTestManager;    //To init

    [SerializeField] AccuracyTestManager accuracyTestManager; //To init

    [SerializeField] int currentTest = 0;
    bool testStarted = false;
    enum Type
    {
        Visual,
        Audio,
        AudioVisual,
        Spatial,
        Accuracy,
        Sequence,
        Last
    }

    bool running = false;

    float maxTestTime = 60;
    [SerializeField] float testTime = 0;

    [SerializeField] bool Finished = false;

    [SerializeField] GameObject gridPanel;

    float waitTime = 0;
    float MaxWaitTime = 0.5f;


    public void StartTest()
    {
        running = true;
    }

    void Init()
    {
        types = new Type[InputCounts];
        scores = new float[InputCounts];

        for (int i = 0; i < InputCounts; i++)
        {
            types[i] = (Type)UnityEngine.Random.Range(0, (int)Type.Last);
        }
    }
    //=========================================================================== V I S U A L 
    void VisualStartInit()
    {
        visualChange.ForeignManager = true;
    }
    void VisualInit()
    {
        visualChange.TimesClicked = 0;
    }
    public void ReactionOnClick()
    {
        waitTime = MaxWaitTime;
        visualChange.Deactivate();
        scores[currentTest] = measureTime.TimesSum;
        measureTime.TimesSum = 0;

        currentTest++;
        testStarted = false;
    }
    void DoVisual()
    {
        if (testStarted)
            return;

        VisualInit();


        testStarted = true;
        visualChange.Activate();
        timingButton.GetComponent<Button>().onClick.RemoveAllListeners();
        timingButton.GetComponent<Button>().onClick.AddListener(ReactionOnClick);

    }

    //=========================================================================== A U D I O
    public void AudioReactionOnClick()
    {
        audioTestManager.OnClick();

        waitTime = MaxWaitTime;

        scores[currentTest] = measureTime.TimesSum;
        measureTime.TimesSum = 0;

        currentTest++;
        testStarted = false;
    }
    void AudioStartInit()
    {
        audioTestManager.ForeignManager = true;
    }
    void AudioInit()
    {
        audioTestManager.TimesClicked = 0;
    }
    void DoAudio()
    {
        if (testStarted)
            return;

        AudioInit();

        testStarted = true;
        audioTestManager.StartTest();
        timingButton.GetComponent<Button>().onClick.RemoveAllListeners();
        timingButton.GetComponent<Button>().onClick.AddListener(AudioReactionOnClick);

    }

    //=========================================================================== A U D I O  & &  V I S U A L 

    public void AudioVisualReactionOnClick()
    {
        visualAudioTestManager.OnClick();

        waitTime = MaxWaitTime;

        scores[currentTest] = measureTime.TimesSum;
        measureTime.TimesSum = 0;

        currentTest++;
        testStarted = false;
    }
    void AudioVisualStartInit()
    {
        visualAudioTestManager.ForeignManager = true;
    }
    void AudioVisualInit()
    {
        visualAudioTestManager.testIndex = 0;
    }
    void DoAudioVisual()
    {
        if (testStarted)
            return;

        AudioVisualInit();

        testStarted = true;
        visualAudioTestManager.TestStartEffect();
        timingButton.GetComponent<Button>().onClick.RemoveAllListeners();
        timingButton.GetComponent<Button>().onClick.AddListener(AudioVisualReactionOnClick);

    }

    //=========================================================================== S P A T I A L
    void SpatialCleanUp()
    {
        spatialTestManager.CleanAndStartNextLevel();
        for (int i = 0; i < spatialTestManager.fields.Length; i++)
        {
            spatialTestManager.fields[i].GetComponent<Image>().color = Color.white;
            spatialTestManager.fields[i].GetComponent<SpatialFieldComponent>().Clicked = false;
        }
    }
    void DoSpatial()
    {
        if(!testStarted)
        {
            SpatialInit();
            spatialTestManager.GenerateClicks();

        }
        testStarted = true;

        if (spatialTestManager.seeTimeLeft > 0)
        {
            spatialTestManager.seeTimeLeft -= Time.deltaTime;
            spatialTestManager.IgnoreInput();
        }

        spatialTestManager.CheckClicks();
        spatialTestManager.VisualizeClicks();
        spatialTestManager.CheckWinCondition();
        
        if(spatialTestManager.Finish)
        {
            scores[currentTest] = spatialTestManager.score;

            currentTest++;
            testStarted = false;
            SpatialCleanUp();
            waitTime = MaxWaitTime;
        }
        if(spatialTestManager.CheckForFail())
        {
            scores[currentTest] = spatialTestManager.score;

            currentTest++;
            testStarted = false;
            SpatialCleanUp();
            waitTime = MaxWaitTime;
        }
    }
    void SpatialInit()
    {
        spatialTestManager.numberOfSquares = 6;
        spatialTestManager.Finish = false;
    }
    void SpatialStartInit()
    {
        spatialTestManager.ForeignManager = true;
    }

    //=========================================================================== S E Q U E N C E this
    void DoSequence()
    {
        if (!testStarted)
        {
            SequenceInit();
            sequenceTestManager.running = true;
            sequenceTestManager.VisualizeClicks();

        }
        testStarted = true;

        if(sequenceTestManager.OneTestFinish) //this
        {
            scores[currentTest] = sequenceTestManager.score;



            currentTest++;
            testStarted = false;
            sequenceTestManager.CleanAndStartNextLevel();
            sequenceTestManager.sequanceIndexes.Clear();

            waitTime = MaxWaitTime;
        }
    }
    void SequenceInit()
    {
        for (int i = 0; i < 3; i++)
        {
            sequenceTestManager.GenerateClicks();
        }
        sequenceTestManager.ShowTime = 0.7f;
        sequenceTestManager.shownID = 0;
        sequenceTestManager.OneTestFinish = false;
    }
    void SequenceStartInit()
    {
        sequenceTestManager.ForeignManager = true;
    }

    //=========================================================================== A C C U R A C Y

    void DoAccuracy()
    {
        int accClicksPerTest = 10;
        if(!testStarted)
        {
            AccuracyInit();
            accuracyTestManager.GenerateButton(200,200);
            measureTime.StartMeasure();
        }
        testStarted = true;

        if(accuracyTestManager.count >= accClicksPerTest)
        {
            accuracyTestManager.FinishTest();
            measureTime.StopMeasure();

            scores[currentTest] = measureTime.TimesSum;
            measureTime.TimesSum = 0;

            currentTest++;
            testStarted = false;
            waitTime = MaxWaitTime;
        }
    }
    void AccuracyInit()
    {
        
    }
    void AccuracyStartInit()
    {
        accuracyTestManager.ForeignManager = true;
    }


    //=========================================================================== M A I N

    private void Start()
    {
        VisualStartInit();
        AudioStartInit();
        AudioVisualStartInit();
        SpatialStartInit();
        SequenceStartInit();
        AccuracyStartInit();
        Init();
        testTime = maxTestTime;
    }
    void DoTest()
    {
        if (currentTest >= types.Length)
            return;


        switch(types[currentTest])
        {
            case Type.Visual:
                DoVisual();
                timingButton.SetActive(true);
                accPanel.SetActive(false);
                gridPanel.SetActive(false);
                break;

            case Type.AudioVisual:
                DoAudioVisual();
                timingButton.SetActive(true);
                accPanel.SetActive(false);
                gridPanel.SetActive(false);
                break;

            case Type.Audio:
                DoAudio();
                timingButton.SetActive(true);
                accPanel.SetActive(false);
                gridPanel.SetActive(false);
                break;

            case Type.Spatial:
                DoSpatial();
                timingButton.SetActive(false);
                accPanel.SetActive(false);
                gridPanel.SetActive(true);
                break;

            case Type.Sequence:
                DoSequence();
                timingButton.SetActive(false);
                accPanel.SetActive(false);
                gridPanel.SetActive(true);
                break;

            case Type.Accuracy:
                DoAccuracy();
                timingButton.SetActive(false);
                accPanel.SetActive(true);
                gridPanel.SetActive(false);
                break;
        }
    }
    private void Update()
    {
        if (!running || Finished)
            return;


        if (testTime <= 0)
        {
            Finished = true;
            accPanel.SetActive(false);
            gridPanel.SetActive(false);
            timingButton.SetActive(false);
        }
        else
        {
            testTime -= Time.deltaTime;
        }

        if(waitTime>0)
        {
            accPanel.SetActive(false);
            gridPanel.SetActive(false);
            timingButton.SetActive(false);

            waitTime -= Time.deltaTime;
            return;
        }

        DoTest();


    }


}
