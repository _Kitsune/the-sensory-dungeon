using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VisualChange : MonoBehaviour
{
    public bool ForeignManager = false;         //Czy chcemy kontrolowa� ten skrypt z innego skryptu
    public Color BaseColor = Color.green;       //Kolor kiedy nie klikamy
    public Color ActiveColor = Color.red;       //Kolor kiedy musimy klikn��
    public float MiniumWaitTime = 0.3f;         //Minimalny czas kt�ry powini�my czeka� mi�dzy klikni�ciami
    public float MaximumWaitTime = 2.0f;        //Minimalny czas kt�ry powini�my czeka� mi�dzy klikni�ciami
    public int HowManyClicks = 10;              //Z ilu klikni�� sk�ada si� test

    [HideInInspector] public int TimesClicked;   //Ile razu zosta�o ju� klikni�te


    [SerializeField] Image buttonImage;           //Image kt�rego kolor b�dziemy zmienia�
    [SerializeField] MeasureTime measureTime;     //Skrypt kt�ry mierzy czas, stoper
    FightVisualSimulation fightVisualSimulation;  //Skrypt od miecza i tarczy

     
    float timeElapsed = -1;                       //Ile czasu musi min��




    /// <summary>
    /// Aktywujemy bloczek by przyjmowa� klikni�cia i mierzy� czas pomi�dzy nimi
    /// </summary>
    public void Activate()
    {
        buttonImage.color = ActiveColor;
        measureTime.StartMeasure();
        fightVisualSimulation.Activate();
    }

    /// <summary>
    /// Obiekt zosta� klikni�ty, zwi�kszamy liczbe klikni��, ustawiamy nowy czas, zmiana koloru i zatrzymanie stopera
    /// </summary>
    public void Deactivate()
    {
        if (timeElapsed != -1 && !ForeignManager)
        {
            timeElapsed = UnityEngine.Random.Range(MiniumWaitTime, MaximumWaitTime);
            return;
        }
            

        TimesClicked++;
        timeElapsed = 0;
        measureTime.StopMeasure();
        buttonImage.color = BaseColor;
    }

    private void Start()
    {
        buttonImage = gameObject.GetComponent<Image>();
        measureTime = gameObject.GetComponent<MeasureTime>();
        fightVisualSimulation = GetComponent<FightVisualSimulation>();
    }

    private void Update()
    {
        if(ForeignManager)
            return;

        if (timeElapsed == -1)
            return;

        if(timeElapsed == 0)
        {
            timeElapsed = UnityEngine.Random.Range(MiniumWaitTime, MaximumWaitTime);
            return;
        }
        if (TimesClicked >= HowManyClicks)
            return;

        timeElapsed -= Time.deltaTime;

        if(timeElapsed <= 0)
        {
            timeElapsed = -1;
            Activate();
        }
    }



}
