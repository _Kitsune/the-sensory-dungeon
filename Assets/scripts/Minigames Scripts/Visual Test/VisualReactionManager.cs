using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VisualReactionManager : MonoBehaviour
{
    [SerializeField] LocalizedStringHandler avarageClickString; //Tekst ze �redni� na koncy testu
    [SerializeField] LocalizedStringHandler scoreString;        //Punkty tekst

    public int ClickNumber = 10;                                //Ile razy ma by� klikni�te
    [SerializeField]TextMeshProUGUI textTime;                   //Obiekt w kt�rym wy�wietlimy czas       
    [SerializeField] TextMeshProUGUI toClear;                   //Obiekt z "pozosta�o" kt�ry mamy usun��
    [SerializeField] TextMeshProUGUI timesClickes;              //Obiekt w kt�rym wy�wietlimy ile razy klikn�li�my

    [SerializeField] GameObject finishScreen;                   //Object with all of the finish panel elements
    [SerializeField] TextMeshProUGUI scoreText;                 //TMpro in which we show level score

    MeasureTime measureTime;                                    //Skrypt stoper 
    VisualChange visualChange;                                  //Skrypt od zmian wizualnych

    bool resultSet = false;                                     //Were the results already placed in the database


    private void Start()
    {
        measureTime = GetComponent<MeasureTime>();
        visualChange = GetComponent<VisualChange>();
  
        avarageClickString.Init();
        scoreString.Init();
    }

    private void Update()
    {

        timesClickes.text = visualChange.TimesClicked.ToString();
        //Jezeli sko�czyli�my test
        if (visualChange.TimesClicked>=visualChange.HowManyClicks)
        {
            toClear.SetText(avarageClickString.UsageString + (measureTime.TimesSum/visualChange.HowManyClicks).ToString() + " ms");
            textTime.gameObject.SetActive(false);

            float avg = (measureTime.TimesSum / visualChange.HowManyClicks);
            int additionalPoints = (int)(100 / (avg / 100));
            finishScreen.SetActive(true);


            if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
            {
                scoreText.SetText(scoreString.UsageString + additionalPoints);
                return;
            }
                

            if (resultSet)
                return;

            GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();

            if(GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount>0)
            {
                additionalPoints = (int)(additionalPoints/2);
            }

            scoreText.SetText(scoreString.UsageString + additionalPoints);
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

            dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
            StartCoroutine(temp_manager.UpdatePoints(additionalPoints));

            resultSet = true;



            return;
        }
        textTime.SetText(measureTime.TimeElapsed.ToString() + " ms");
        
    }

}
