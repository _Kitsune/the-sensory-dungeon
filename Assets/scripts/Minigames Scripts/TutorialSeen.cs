using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSeen : MonoBehaviour
{
    [SerializeField] string tutorialName;           //Tutorial name, will be in playerprefs
    [SerializeField] GameObject tutorialObject;     //Object with the tutorial

    //Tutorial object was clicked
    public void Clicked()
    {
        PlayerPrefs.SetInt(tutorialName, 1);
    }

    private void Start()
    {
        //we already showed it so we won't show it again
        if(PlayerPrefs.HasKey(tutorialName))
        {
            tutorialObject.SetActive(false);
        }
    }
}
