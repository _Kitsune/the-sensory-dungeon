using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoxRunner : MonoBehaviour
{
    /// <summary>
    /// The fox has to run somewhere
    /// </summary>
    public void OnClick()
    {
        //getting first and only fox, but the fox could be not active, thats why this way
        FoxRunManager[] a = Resources.FindObjectsOfTypeAll<FoxRunManager>();
        GameObject fox = a[0].gameObject;

        //If fox haven't disappeared, change the position
        //prevents from running when fox should be inactive
        if(!fox.GetComponent<FoxRunManager>().Disappear)
        {
            fox.SetActive(true);
            fox.GetComponent<FoxRunManager>().Position = new Vector2(GetComponent<RectTransform>().localPosition.x, GetComponent<RectTransform>().localPosition.y);
        }
    }


}
