using System.Collections.Generic;
using System.Security;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SequenceTestManager : MonoBehaviour
{
    [SerializeField] Sprite baseSprite;                         //Bazowy sprite, nieaktywny
    [SerializeField] Sprite activeSprite;                       //Sprite aktywowany, podczas pokazaywania sekwencji

    [SerializeField] LocalizedStringHandler levelString;        //Tekst kt�ry pokazuje kt�ry mamy poziom
    [SerializeField] LocalizedStringHandler timeFinishString;   //Tekst kiedy sko�czyli�my
    [SerializeField] LocalizedStringHandler secondsString;      //Sekundy w dobrym j�zyku
    [SerializeField] LocalizedStringHandler scoreString;        //Tekst do wyniku


    [SerializeField] TextMeshProUGUI timerObject;       //Obiekt w kt�rym b�dziemy pokazywa� czas kt�ry zosta�
    [SerializeField] TextMeshProUGUI finalTimerObject;  //Obiekt w kt�rym b�dziemy �rednia uzyskanego czasu 
    [SerializeField] TextMeshProUGUI scoreObject;       //Obiekt w kt�rym b�dziemy pokazywa� uzyskany poziom
    [SerializeField] GameObject panel;                  //Panel posiadaj�cy grid z blokami kt�re b�dziemy klika�
    [SerializeField] public GameObject[] fields;               //Guzika z grida, kt�re b�dziemy klika�
    [SerializeField] GameObject[] wrongAnswerObjects;   //Obiekty kt�re si� pokazuj� w przypadku klikni�cia nie poprawnej odpowiedzi

    [SerializeField]public float TimeToWait = 0.1f;     //Ile czasu musimy czeka� na kolejny blok sekwencji
    [SerializeField]public float ShowTime = 0.5f;       //Ile b�dziemy ogl�da� jeden blok sekwencji
    [SerializeField]public float MaxTime = 40;          //Czas trwania testu

    [SerializeField] GameObject[] showHideWithTest;     //Objects we will show and hide when the test starts/ends

    [SerializeField] GameObject fox;                    //Fox object, he do be running

    [SerializeField] GameObject finishScreen;           //Panel in which we show score etc
    [SerializeField] TextMeshProUGUI endScoreText;      //We show score here


    public int score = 1;                               //Obecny poziom, wynik 
    int wrongAnswers = 0;                               //Obecna ilo�� niepoprawnych odpowiedzi

    public bool running = false;                        //Czy skrypt ma chodzi�, test trwa
    bool visualize = false;                             //Czy pokazujemy w�a�nie sekwencje
    bool waited = false;                                //Czy ju� przeczekali�my czas pomi�dzy pokazaniem jednego bloku sekwencji a nast�pnego

    public List<int> sequanceIndexes = new List<int>(); //Obecna sekwencja (po id z fields)
    int CurrentSequenceElementId = 0;                   //ID w sekwencji guzika na kt�ry czekamu

    float timeLeft = 0;                                 //Ile obecnie trwa test
    float timeWaited = -1;                              //Ile ju� czekali�my tej przerwy [TimeToWait]. -1 jako musimy znowu zacz�� czeka�
    float showedTime = 1.5f;                            //Ile ju� ogl�damy blok sekwencji

    public int shownID = 0;                             //Kt�re ID sekwencji pokazujemy kiedy j� wy�wietlamy

    public bool ForeignManager=false;                   //Czy sterujemy tym skryptem z innego skryptu
    public bool OneTestFinish = false;                  //Czy sko�czyli�my jeden test w przypadku prowadzenia go z innego skryptu




    /// <summary>
    /// Rozpoczyna test - Pokazuje grid i wybiera bloki do klikni�cia
    /// </summary>
    public void StartTest()
    {
        for(int i = 0; i < showHideWithTest.Length; i++)
        {
            showHideWithTest[i].SetActive(true);
        }
        running = true;
        panel.SetActive(true);
        GenerateClicks();
        VisualizeClicks();
    }

    /// <summary>
    /// Dodaje jeden losowy blok do sekwencji
    /// </summary>
    public void GenerateClicks()
    {
        int randomIndex = UnityEngine.Random.Range(0, fields.Length-1);

        sequanceIndexes.Add(randomIndex);
    }


    /// <summary>
    /// Ustawia opcje wizualizowania klikni��
    /// visualize = true
    /// </summary>
    public void VisualizeClicks()
    {
        visualize = true;
    }

    /// <summary>
    /// zaczyna kolejny poziom
    /// </summary>
    void GenerateNewLevel()
    {
        StartTest();
    }

    /// <summary>
    /// Czy�ci pozosta�o�ci tego poziomu i rozpoczyna nowy
    /// zwi�ksza wynik i go pokazuje
    /// </summary>
    public void CleanAndStartNextLevel()
    {
        //Chowanie obiekt�w z�ych klikni��
        for (int i = 0; i < wrongAnswerObjects.Length; i++)
        {
            wrongAnswerObjects[i].SetActive(false);
        }

        fox.GetComponent<FoxRunManager>().Disappear = true;

        waited = false;
        TimeToWait = 0.1f;
        timeWaited = -1;
        shownID = 0;
        ShowTime = 1;
        showedTime = ShowTime;
        CurrentSequenceElementId = 0;                       

        wrongAnswers = 0;                                   //Zeruje liczb� niepoprawnych odpowiedzi
        score++;                                            //Zwi�ksza wynik, poziom na kt�rym jeste�my
        if(!ForeignManager)
        {
            GenerateNewLevel();                                 //Tworzy nowy poziom
            scoreObject.SetText(levelString.UsageString + score.ToString());  //Pokazuje na kt�rym poziomie jeste�my
        }
            
    }


    /// <summary>
    /// Sprawdza czy klikn�li�my wszystkie elementy sekwencji
    /// </summary>
    void CheckWinCondition()
    {
        if (CurrentSequenceElementId < sequanceIndexes.Count)
            return;

        if(ForeignManager)
        {
            running = false;
            OneTestFinish = true;
            score = 1;
            return;
        }

        CleanAndStartNextLevel();
 
    }

    /// <summary>
    /// Uaktywnia obiekty kt�re pokazuj� u�ytkownikowi ile ma z�ych odpowiedzi
    /// Tylko jedn�, o id = {wrongAnswers} - 1
    /// </summary>
    void UpdateWrongAnswers()
    {
        if (wrongAnswers <= wrongAnswerObjects.Length)
            wrongAnswerObjects[wrongAnswers - 1].SetActive(true);
    }


    /// <summary>
    /// Sprawdza czy klikn�li�my kt�ry� guzik, je�li tak to czy dobrze
    /// </summary>
    void CheckClicks()
    {
        for (int i = 0; i < fields.Length; i++)
        {
            if (fields[i].GetComponent<SpatialFieldComponent>().Clicked) //Sprawdzamy czy guzik zosta� klikni�ty
            {
                fields[i].GetComponent<SpatialFieldComponent>().Clicked = false; //Zmieniamy by nie bra� tego samego guzika dwa razy pod uwag�

               if(i == sequanceIndexes[CurrentSequenceElementId]) //sprawdzamy czy klikn�li�my to id kt�re teraz mieli�my
                {
                    CurrentSequenceElementId++;
                    return;
                }

                wrongAnswers++;
                UpdateWrongAnswers();
                return;

            }
        }
    }

    /// <summary>
    /// Zako�czenie testu
    /// Wy��czenie skrypty, panelu i zmiana napisu w stoperze
    /// </summary>
    void FinishTest()
    {
        if(ForeignManager)
        {
            running = false;
            OneTestFinish = true;
            score = -1;
            return;
        }
        running = false;
        panel.SetActive(false);
        for (int i = 0; i < showHideWithTest.Length; i++)
        {
            showHideWithTest[i].SetActive(false);
        }
        finalTimerObject.GetComponent<TextMeshProUGUI>().SetText(timeFinishString.UsageString);
        timerObject.GetComponent<TextMeshProUGUI>().SetText("");


        int additionalPoints = 0;
        if (wrongAnswers>=3)
        {
            additionalPoints = score * 2;
        }
        else
        {
            additionalPoints = score * 4;
        }
        
        finishScreen.SetActive(true);

        if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
        {
            endScoreText.SetText(scoreString.UsageString + additionalPoints);
            return;
        }

        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount > 0)
        {
            additionalPoints = (int)(additionalPoints / 2);
        }

        endScoreText.SetText(scoreString.UsageString + additionalPoints);
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

        dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
        StartCoroutine(temp_manager.UpdatePoints(additionalPoints));


        GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();

    }

    /// <summary>
    /// Zmienia blok do stanu bazowego
    /// </summary>
    void HideEffect()
    {
        fields[sequanceIndexes[shownID]].GetComponent<Image>().sprite = baseSprite;
    }
    
    /// <summary>
    /// Zmienia blok do stanu kt�ry w kt�rym u�ytkownik widzi �e b�dzie mia� go klikn��
    /// </summary>
    void ShowEffect()
    {
        fields[sequanceIndexes[shownID]].GetComponent<Image>().sprite = activeSprite;
    }
    
   /// <summary>
   /// Pokazuje sekwencje
   /// </summary>
    void ShowSeqeunce()
    {
        if(showedTime > 0) //Sprawdzamy czy dalej mamy pokazywa� blok
        {
            ShowEffect();
            showedTime -= Time.deltaTime;
            return;
        }
      
        if(timeWaited == -1) //Sprawdzamy czy rozpoczynamy czekanie przerwy mi�dzy pokazaniem bloku sekwencji jednego i drugiego
        {
             HideEffect();
             timeWaited = TimeToWait;
            
             return;
        }
            
        if(timeWaited > 0) //Sprawdzamy czy dalej mamy czeka� przerw� mi�dzy pokazaniem blok�w sekwencji
        {
             timeWaited -= Time.deltaTime;
             return;
        }

        shownID++;      //Zwi�kszamy id bloku kt�ry chcemy pokaza�

        if (shownID >= sequanceIndexes.Count) //Sprawdzamy czy pokazali�my wszystkie bloki
        {
            visualize = false;
            fox.GetComponent<FoxRunManager>().Disappear = false;
            return;
        }

        //Pokazujemy nowy element i resetujemy potrzebne zmienne

        ShowEffect();
        showedTime = ShowTime;
        timeWaited = -1;

    }

    private void Start()
    {
        //Przygotowanie zmiennych
        timeLeft = MaxTime;

        timeFinishString.Init();
        levelString.Init();
        secondsString.Init();

        //Stworzenie pierwszego bloku sekwencji (By�my zaczynali od dw�ch klikni��)
        GenerateClicks();
    }

    private void Update()
    {
        if (!running || OneTestFinish) //Wychodzimy kiedy ma nie chodzi�
            return;


        if (visualize) //czy pokazujemy sekwencje
        {
            ShowSeqeunce(); //poka� co trzeba

            for (int i = 0; i < fields.Length; i++) //Ignorujemy klikni�cia u�ytkownika
            {
                fields[i].GetComponent<SpatialFieldComponent>().Clicked = false;
            }
            return;
        }


        CheckClicks();              //Sprawdzamy klikni�cia
        CheckWinCondition();        //Sprawdzamy czy u�ytkownik przeszed� poziom

        //zmniejsz czas
        if(!ForeignManager)
            timeLeft -= Time.deltaTime;


        //Ko�czy test po czasie i wychodzi by nie nadpisa� tekstu w timerObject
        if (timeLeft <= 0 && !ForeignManager)
        {
            FinishTest();
            return;
        }

        //Ko�czy test po ilo�ci zlych odpowiedzi i wychodzi by nie nadpisa� tekstu w timerObject
        if (wrongAnswers >= 3)
        {
            FinishTest();
            return;
        }

        //pokazuje pozosta�y czas
        if(!ForeignManager)
           timerObject.GetComponent<TextMeshProUGUI>().SetText(System.Math.Round(timeLeft, 3).ToString() + " " + secondsString.UsageString);

    }


}
