using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using Unity.VisualScripting;
using UnityEngine;

public class FoxRunManager : MonoBehaviour
{
    public Vector2 Position;                        //Position to which our fox runs
    [SerializeField]float speed = 100.0f;           //Speed with which fox runs

    public bool Disappear = true;                   //Should he disappear after getting into the destination

    void Update()
    {
        //Getting normalized direction from fox to destination and how far it is {diff}
        Vector2 Dir = new Vector2(transform.localPosition.x,transform.localPosition.y) - Position;
        Vector2 diff = Dir;
        Dir.Normalize();


        if (Mathf.Abs(diff.x) + Mathf.Abs(diff.y) > 40) //if we are close enough (40 cause that worked, 10 was to little, and 20 didn't work on one device)
        {
            //Move and rotate the fox in good dir
            transform.localPosition += new Vector3((Dir * speed * Time.deltaTime).x, (Dir * speed * Time.deltaTime).y, 0) * -1;
            float angle = Mathf.Atan2(Dir.x, Dir.y) * 180 / Mathf.PI;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -angle);
            transform.Rotate(0, 0, 180);
        }else if(Disappear)
        {
            //Disapper the fox if fox should
            gameObject.SetActive(false);
        }
        

    }
}
