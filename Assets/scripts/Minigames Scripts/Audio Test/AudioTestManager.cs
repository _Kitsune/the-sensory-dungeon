using System.Collections;
using System.Collections.Generic;
using System.Security;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class AudioTestManager : MonoBehaviour
{
    [SerializeField] LocalizedStringHandler avarageClickString; //Tekst ze �redni� - pojawi si� na ko�cu
    [SerializeField] LocalizedStringHandler scoreString;         //Tekst do wyniku
    [SerializeField] TextMeshProUGUI text;                       //Obiekt w kt�rym wy�wietlimy tekst     
    [SerializeField] TextMeshProUGUI scoreText;                  //Obiekt w kt�rym piszemu to klikni�cie  
    [SerializeField] TextMeshProUGUI finalTimerText;             //Obiekt z "pozostalo"  

    [SerializeField] MeasureTime timer;                         //Skrypt stopera
    [SerializeField] AudioClip toClickSound;                    //D�wi�k kt�ry si� odpali kiedy mamy klikn��
    [SerializeField] AudioSource audioPlayer;                   //Komponent kt�ry odtworzy d�wi�k

    [SerializeField] GameObject finishScreen;                   //Finish object - with some scores
    [SerializeField] TextMeshProUGUI endScoreText;              //Text in which we will state the score of a level


    public float MiniumWaitTime = 0.3f;         //Minimalny czas kt�ry powini�my czeka� mi�dzy klikni�ciami
    public float MaximumWaitTime = 2.0f;        //Minimalny czas kt�ry powini�my czeka� mi�dzy klikni�ciami
    public int HowManyClicks = 10;              //Z ilu klikni�� sk�ada si� test

    [HideInInspector] public int TimesClicked;  //Ile razu zosta�o ju� klikni�te

    float timeElapsed = 0;                      //Ile min�o czasu pomi�dzy testami          

    bool running = false;                       //Czy skrypt chodzi, zosta� uruchomiony
    bool finished = false;                      //Czy test si� zako�czy� 
    bool waiting = false;                       //Czy w�a�nie czekamy na kolejny test

    public bool ForeignManager = false;         //Czy u�ywamy tylko funkcji ze skryotu nadzoruj�c test z innego skryptu

    FightVisualSimulation fvs;                  //Skrypt prezentacji walki

    /// <summary>
    /// Ustawia {timeElapsed} timer na dobry czas
    /// </summary>
    void GenerateLevel()
    {
        timeElapsed = UnityEngine.Random.Range(MiniumWaitTime, MaximumWaitTime);
        waiting = false;
    }

    /// <summary>
    /// Skrypt pierwszego odwarcia testu
    /// obiekt startu wy��czany, generowanie
    /// </summary>
    void InitTest()
    {
        fvs = GetComponent<FightVisualSimulation>();
        running = true;
        GenerateLevel();
    }
    
    /// <summary>
    /// Funkcja na klikni�cie
    /// init za pierwszym razem
    /// reset timera na z�e klikni�cie
    /// nast�pny poziom na dobre
    /// </summary>
    public void OnClick()
    {
        if (finished)
            return;

        if (!running && !ForeignManager)   //pierwsze odpalenie
        {
            InitTest();
            return;
        }

        if(timeElapsed>0 && !ForeignManager)   //nie powini�my byli klikn�� a wi�c resetuje timer
        {
            GenerateLevel();
            return;
        }

        //powini�my byli klikn��
        TimesClicked++;
        scoreText.SetText(TimesClicked.ToString());
        timer.StopMeasure();
        GenerateLevel();
        

    }

    /// <summary>
    /// rozpoczyna moment testu, odtwarza d�wi�k w dobrym momencie i zaczyna mierzy� czas
    /// </summary>
    public void StartTest()
    {
        if (waiting)
            return;


        audioPlayer.Play();
        timer.StartMeasure();
        fvs.Activate();
    }
    
    /// <summary>
    /// Zaka�cza test
    /// Wy�wietla wynik = �redni�
    /// </summary>
    void Finish()
    {
        finalTimerText.SetText(avarageClickString.UsageString + (timer.TimesSum / HowManyClicks).ToString() + " ms");
        text.SetText("");

        float avg = (timer.TimesSum / HowManyClicks);
        int additionalPoints = (int)(100 / (avg / 100));
        finishScreen.SetActive(true);

        if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
        {
            endScoreText.SetText(scoreString.UsageString + additionalPoints);
            return;
        }

        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount > 0)
        {
            additionalPoints = (int)(additionalPoints / 2);
        }

        endScoreText.SetText(scoreString.UsageString + additionalPoints);
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

        dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
        StartCoroutine(temp_manager.UpdatePoints(additionalPoints));


        GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();
    }


    private void Update()
    {
        if (!running || finished)
            return;

        if(timeElapsed<=0)  //Zaczynamy jeden pomiar
        {
            StartTest();
            waiting = true;
        }
        else                //zmniejszamy czas, do pomiaru
        {
            timeElapsed -= Time.deltaTime;
        }

        //Sprawdzamy czy to koniec
        if(TimesClicked>=HowManyClicks)
        {
            finished = true;
            Finish();
            return;
        }

        //ustawiamy tekst w timerze
        text.SetText(timer.TimeElapsed.ToString() + " ms");

    }





    private void Start()
    {
        timer = GetComponent<MeasureTime>();
        audioPlayer.clip = toClickSound;
        

        avarageClickString.Init();
        scoreString.Init();
    }


}
