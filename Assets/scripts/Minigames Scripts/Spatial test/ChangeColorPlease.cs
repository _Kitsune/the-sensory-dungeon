using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColorPlease : MonoBehaviour
{
    /// <summary>
    /// Zmienia kolor komponentu image na {New} je�li ten kolor nie by� {Correct}.
    /// W przypadku Forcefully jako true, zmienia kolor z komponentu na {New}
    /// </summary>
    /// <param name="New">Kolor na kt�ry chcemy zmieni�</param>
    /// <param name="Correct">Kolor kt�tego nie chcemy zmienia�</param>
    /// <param name="Forcefully">Czy mamy zmienia� nie wa�ne co</param>
    public void PLeaseChangeColor(Color New, Color Correct, bool Forcefully)
    {
        //sprawdzamy czy musimy sprawdza� kolory
        if(Forcefully)
        {
            GetComponent<Image>().color = New;
            return;
        }
        //Sprawdzamy czy kolor kt�ry jest, jest taki kt�rego nie chcemy zmieni�
        if (GetComponent<Image>().color == Correct)
            return;

        //Zmieniamy kolor
        GetComponent<Image>().color = New;
    }

    /// <summary>
    /// Zmienia sprite komponentu image na {New} je�li ten sprite nie by� {Correct}.
    /// W przypadku Forcefully jako true, zmienia sprite z komponentu na {New}
    /// </summary>
    /// <param name="New">Sprite na kt�ry chcemy zmieni�</param>
    /// <param name="Correct">Sprite kt�tego nie chcemy zmienia�</param>
    /// <param name="Forcefully">Czy mamy zmienia� nie wa�ne co</param>
    public void PleaseChangeSprite(Sprite New, Sprite Correct, bool Forcefully)
    {
        //sprawdzamy czy musimy sprawdza� kolory
        if (Forcefully)
        {
            GetComponent<Image>().sprite = New;
            return;
        }
        //Sprawdzamy czy kolor kt�ry jest, jest taki kt�rego nie chcemy zmieni�
        if (GetComponent<Image>().sprite == Correct)
            return;

        //Zmieniamy kolor
        GetComponent<Image>().sprite = New;
    }
}
