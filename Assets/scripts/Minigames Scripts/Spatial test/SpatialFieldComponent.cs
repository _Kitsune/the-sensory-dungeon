using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpatialFieldComponent : MonoBehaviour
{
    [HideInInspector]public bool Clicked;

    /// <summary>
    /// Zmienia warto�� Clicked na true
    /// </summary>
    public void Change()
    {
        Clicked = true;
    }
}
