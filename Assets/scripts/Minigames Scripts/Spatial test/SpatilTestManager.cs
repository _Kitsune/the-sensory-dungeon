using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Data;
using Unity.VisualScripting;
using System.Security;

public class SpatilTestManager : MonoBehaviour
{
    [SerializeField] Sprite wrongChestSprite;                   //Sprite for a wrong answer
    [SerializeField] Sprite correctChestSprite;                 //Sprite for an good answer
    [SerializeField] Sprite baseChestSprite;                    //Basic sprite

    [SerializeField] LocalizedStringHandler levelString;        //Tekst kt�ry pokazuje kt�ry mamy poziom
    [SerializeField] LocalizedStringHandler timeFinishString;   //Tekst kiedy sko�czyli�my
    [SerializeField] LocalizedStringHandler secondsString;      //Sekundy w dobrym j�zyku
    [SerializeField] LocalizedStringHandler scoreString;        //Tekst do wyniku

    [SerializeField] TextMeshProUGUI timerObject;       //Obiekt w kt�rym b�dziemy pokazywa� czas kt�ry zosta�
    [SerializeField] TextMeshProUGUI finalTimerObject;  //Obiekt w kt�rym b�dziemy pokazywa� �redni czas
    [SerializeField] TextMeshProUGUI scoreObject;       //Obiekt w kt�rym b�dziemy pokazywa� uzyskany poziom
    [SerializeField] GameObject panel;                  //Panel posiadaj�cy grid z blokami kt�re b�dziemy klika�
    [SerializeField] public GameObject[] fields;        //Guzika z grida, kt�re b�dziemy klika�
    [SerializeField] GameObject[] wrongAnswerObjects;   //Obiekty kt�re si� pokazuj� w przypadku klikni�cia nie poprawnej odpowiedzi

    [SerializeField] GameObject[] showHideWithTest;     //Objects we will show/hide on start/finish of the test

    [SerializeField] GameObject finishScreen;           //Object with all of the texts, panels etc which we show at the end, with score
    [SerializeField] TextMeshProUGUI endScoreText;      //Text object in which we show the score after each level

    bool[,] clicked;                                    //Tablica kt�ra przechowuje informacje kt�re bloki powinny zosta� klikni�te [x,0], a kt�re zosta�y klikni�te [x,1]

    public int MaxNumberOfSquares = 16;                 //Maksymalna liczba kwadrat�w do klikni�cia w jednym momencie

    public float MaxTime = 30;                          //Czas trwania testu
    public float SeeTime = 2;                           //Czas w kt�rym pokazuj� si� bloczki kt�re mamy klikn�� - nie zmniejsza czasu trwania testu

    float timeLeft = 0;                                 //Czas pozosta�y testu
    public float seeTimeLeft = 0;                       //Czas pozosta�y pokazywania si� poprawnych bloczk�w

    public int numberOfSquares = 2;                     //Obecna ilo�� kwadrat�w do klikni�cia
    public int score = 0;                               //Obecny poziom, wynik = (numberOfSquares-2)
    int wrongAnswers = 0;                               //Obecna ilo�� niepoprawnych odpowiedzi

    bool running = false;                               //Czy skrypt i chodzi�, test trwa
    bool visualizeWithForce = false;                    //Czy ma nadpisa� poprawnie pokazane bloczki (z kolorem ClickedCorrect). True - kiedy u�ytkownik wchodzi na wy�szy poziom

    public bool ForeignManager = false;                 //Czy sterujemy funkcjami, testem z innego skryptu
    public bool Finish = false;                         //Czy zrobili�my jeden test, kiedy u�ywamy skryptu z zewn�trz

    bool once = false;                                  //Stores the value for cleaning the level, like visualize with force


    /// <summary>
    /// Rozpoczyna test - Pokazuje grid i wybiera bloki do klikni�cia
    /// </summary>
    public void StartTest()
    {
        for (int i = 0; i < showHideWithTest.Length; i++)
        {
            showHideWithTest[i].SetActive(true);
        }
        running = true;
        panel.SetActive(true);
        GenerateClicks();
        VisualizeClicks();
    }
    
    /// <summary>
    /// Wybiera kt�re bloki maj� by� do klikni�cia i ustawia je w clicked[x,0]
    /// Losowanie bez powt�rze�
    /// </summary>
    public void GenerateClicks()
    {
        //Sprawdzamy czy liczba kwadrat�w kt�r� mamy wygenerowa� nie jest wi�ksza od ustalonej przez nas maksymalnej liczby
        if(numberOfSquares > MaxNumberOfSquares)
            numberOfSquares = MaxNumberOfSquares;

        //tworzymy list� z mo�liwymi do wylosowania indeksami
        List<int> indexes = new List<int>();
        for (int i = 0; i < clicked.GetLength(0); i++)
        {
            indexes.Add(i);
        }

        //Losujemy z wcze�niejszej tablicy a nast�pnie je dodajemy do {randomIndexes}
        List<int> randomIndexes = new List<int>();
        for (int i = 0; i < numberOfSquares; i++)
        {
            int index_temp = UnityEngine.Random.Range(0, indexes.Count);    //Losowanie indeksu
            randomIndexes.Add(indexes[index_temp]);                         //Dodanie go do {randomIndexes}
            indexes.RemoveAt(index_temp);                                   //Usuni�cie mo�liwo�ci wylosowania tego indeksu p�niej, usuwaj�c go z tablicy
        }
        
        //Ustawiamy kt�re bloki maj� by� do klikni�cia z losowych indeks�w
        for (int i = 0; i < randomIndexes.Count; i++)
        {
            clicked[randomIndexes[i], 0] = true;
        }


    }
   
    /// <summary>
    /// Zmienia kolor guzik�w w zale�no�ci od tego czy zosta�y, czy powinny zosta� klikni�te
    /// </summary>
    public void VisualizeClicks()
    {
        for (int i = 0; i < fields.Length; i++)
        {  
            if (!clicked[i,0]) //Kiedy nie powinni�my klikn�� w blok
            {
                if(clicked[i,1]) //A klikn�li�my
                {
                    fields[i].GetComponent<ChangeColorPlease>().PleaseChangeSprite(wrongChestSprite, correctChestSprite, visualizeWithForce);
                    continue;
                }
                //A nie klikn�li�my
                fields[i].GetComponent<ChangeColorPlease>().PleaseChangeSprite(baseChestSprite, correctChestSprite, visualizeWithForce);
            }
            else //Kiedy powinni�my klikn��
            {
                if (clicked[i, 1]) //i to zrobili�my
                {
                    fields[i].GetComponent<ChangeColorPlease>().PleaseChangeSprite(correctChestSprite, correctChestSprite, visualizeWithForce);
                    continue;
                }

                if(seeTimeLeft>0)   //Kiedy powinni�my klikn��, ale jeszcze powini�my widzie� �e tego mamy klikna�
                {
                    fields[i].GetComponent<ChangeColorPlease>().PleaseChangeSprite(correctChestSprite, correctChestSprite, visualizeWithForce);
                }else               //Kiedy powini�my klikn��, ale ju� nie widzimy �e tu
                {
                    fields[i].GetComponent<ChangeColorPlease>().PleaseChangeSprite(baseChestSprite, correctChestSprite, visualizeWithForce);
                }
                
            }
        }
        visualizeWithForce = false; //Nie zmuszaj do zmiany koloru

    }
    
    /// <summary>
    /// Zeruje clicked i zaczyna kolejny poziom
    /// </summary>
    void GenerateNewLevel()
    {
        for (int i = 0; i < fields.Length; i++)
        {
            clicked[i, 0] = false;
            clicked[i, 1] = false;
        }
        if(!ForeignManager)
            StartTest();
    }
   
    /// <summary>
    /// Czy�ci pozosta�o�ci tego poziomu i rozpoczyna nowy
    /// zwi�ksza wynik i go pokazuje
    /// </summary>
    public void CleanAndStartNextLevel()
    {
        //Chowanie obiekt�w z�ych klikni��
        for (int i = 0; i < wrongAnswerObjects.Length; i++)
        {
            wrongAnswerObjects[i].SetActive(false);
        }

        visualizeWithForce = true;                          //Ustawione by zmienia� kolor ka�dego bloku na kolor bazowy 
        wrongAnswers = 0;                                   //Zeruje liczb� niepoprawnych odpowiedzi
        seeTimeLeft = SeeTime;                              //Resetuje stoper widzenia kt�re bloki mamy klikn��
        numberOfSquares++;                                  //Zwi�ksza liczb� pokazywanych blok�w
        score++;                                            //Zwi�ksza wynik, poziom na kt�rym jeste�my
        GenerateNewLevel();                                 //Tworzy nowy poziom

        if(!ForeignManager)
            scoreObject.SetText(levelString.UsageString + score.ToString());  //Pokazuje na kt�rym poziomie jeste�my
    }

    /// <summary>
    /// Sprawdza czy przeszli�my poziom - zaznaczyli�my poprawnie wszystkie bloczki kt�re mieli�my i odpala posprz�tanie po tym poziomie i zacz�cie nowego
    /// Czy wszystkie clicked[x,0] s
    /// </summary>
    public void CheckWinCondition()
    {
        for (int i = 0; i < clicked.GetLength(0); i++)
        {
            if (clicked[i,0])
            {
                return;
            }
        }
        if(ForeignManager)
        {
            Finish = true;
            score = 1;
            return;
        }
        CleanAndStartNextLevel();
    }

    /// <summary>
    /// Uaktywnia obiekty kt�re pokazuj� u�ytkownikowi ile ma z�ych odpowiedzi
    /// Tylko jedn�, o id = {wrongAnswers} - 1
    /// </summary>
    void UpdateWrongAnswers()
    {
        if(wrongAnswers<= wrongAnswerObjects.Length)
            wrongAnswerObjects[wrongAnswers - 1].SetActive(true);
    }
    
    /// <summary>
    /// Sprawdza kt�re bloczki zosta�y klikni�te i aktualizuje do tego clicked[x,1]
    /// </summary>
    public void CheckClicks()
    {
        for (int i = 0; i < fields.Length; i++)
        {
            if(fields[i].GetComponent<SpatialFieldComponent>().Clicked) //Sprawdzamy czy guzik zosta� klikni�ty
            {
                fields[i].GetComponent<SpatialFieldComponent>().Clicked = false; //Zmieniamy by nie bra� tego samego guzika dwa razy pod uwag�

                if (clicked[i,1])           //Je�li zosta� klikni�ty, to zosta� ju� obs�u�ony, wi�c go ignorujemy
                {
                    continue;
                }

                clicked[i,1] = true;       //Ustawiamy �e zosta� klikni�ty

                if (!clicked[i,0])         //Je�li nie powinien by� klikni�ty
                {
                    wrongAnswers++;        //Zwi�kszamy liczb� z�ych klikni��
                    UpdateWrongAnswers();  //Od�wie�amy obiekty kt�re do pokazuj�
                    continue;              //Idziemy dalej
                }
               
                VisualizeClicks();         //pokazujemy �e zosta� klikni�ty poprawnie

                clicked[i, 0] = false;     //Zmieniamy na false by powiedzie�, �e ju� nie musi by� klikni�ty
                
            }
        }
    }
    
    /// <summary>
    /// Zako�czenie testu
    /// Wy��czenie skrypty, panelu i zmiana napisu w stoperze
    /// </summary>
    void FinishTest()
    {
        for (int i = 0; i < showHideWithTest.Length; i++)
        {
            showHideWithTest[i].SetActive(true);
        }
        running = false;
        panel.SetActive(false);
        finalTimerObject.SetText(timeFinishString.UsageString);
        timerObject.SetText("");


        int additionalPoints = 0;
        if (wrongAnswers >= 3)
        {
            additionalPoints = score * 2;
        }
        else
        {
            additionalPoints = score * 4;
        }
        finishScreen.SetActive(true);

        if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
        {
            endScoreText.SetText(scoreString.UsageString + additionalPoints);
            return;
        }

        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount > 0)
        {
            additionalPoints = (int)(additionalPoints / 2);
        }

        endScoreText.SetText(scoreString.UsageString + additionalPoints);
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount++;

        dbmanager temp_manager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
        StartCoroutine(temp_manager.UpdatePoints(additionalPoints));

        GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().Finish();
    }

    /// <summary>
    /// Sprawdza czy przegrali�my, mamy wi�cej ni� 3 b��dne odpowiedzi
    /// </summary>
    /// <returns>True w przypadku przegranej</returns>
    public bool CheckForFail()
    {
        if (wrongAnswers >= 3)
        {
            if(ForeignManager)
            {
                Finish = true;
                score = -1;
                return true;
            }    
            FinishTest();
            return true;
        }
        return false;
    }
    
    
    private void Start()
    {
        timeFinishString.Init();
        levelString.Init();
        secondsString.Init();

        //Pocz�tkowe ustawienia zmiennych
        seeTimeLeft = SeeTime;                  
        timeLeft = MaxTime;
        clicked = new bool[fields.Length, 2];

        //Zerowanie clicked
        for (int i = 0; i < fields.Length; i++)
        {
            clicked[i, 0] = false;
            clicked[i, 1] = false;
        }
    }
    
    /// <summary>
    /// Anuluje klikni�cia u�ytkownika
    /// </summary>
    public void IgnoreInput()
    {
        for (int i = 0; i < fields.Length; i++) //Ignorujemy klikni�cia u�ytkownika
        {
            fields[i].GetComponent<SpatialFieldComponent>().Clicked = false;
        }
    }
    private void Update()
    {

        if(!running) //Wychodzimy kiedy ma nie chodzi�
            return;

        if(seeTimeLeft>0) //Ignorujemy inputy w czasie w kt�rym u�tkownikowi pokazywane s� bloczki kt�re ma klikn��
        {
            seeTimeLeft -= Time.deltaTime;  //Zmniejszamy stoper
            for (int i = 0; i < fields.Length; i++) //Ignorujemy klikni�cia u�ytkownika
            {
                fields[i].GetComponent<SpatialFieldComponent>().Clicked = false;
            }
            once = true;
            return;
        }
        if(once)
        {
            once = false;
            visualizeWithForce = true;
        }

        CheckClicks();
        VisualizeClicks();
        CheckWinCondition();

        //zmniejsz czas
        timeLeft -= Time.deltaTime;


        //Ko�czy test po czasie i wychodzi by nie nadpisa� tekstu w timerObject
        if (timeLeft <= 0)
        {
            FinishTest();
            return;
        }

        //Ko�czy test po ilo�ci zlych odpowiedzi i wychodzi by nie nadpisa� tekstu w timerObject
        if(CheckForFail())
        { 
            return;
        }

        //pokazuje pozosta�y czas
        timerObject.GetComponent<TextMeshProUGUI>().SetText(System.Math.Round(timeLeft, 3).ToString() + " " + secondsString.UsageString);

    }

}
