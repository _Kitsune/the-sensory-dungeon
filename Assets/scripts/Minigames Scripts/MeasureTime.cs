using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System;


/// <summary>
/// S�u�y do mierzenia czasu od StartMeasure() do StopMeasure() w TimeElapsed w milisekundach
/// Sumuje otrzymane wyniki w TimesSum
/// Zaokr�gla czas do Precision miejsc po przecinku
/// </summary>
public class MeasureTime : MonoBehaviour
{

    bool measure = false;                       //Czy skrypt ma dzia�a�
    public float TimeElapsed = 0;               //Ile czasu odp�yn�o od rozpocz�cia mierzenia
    [HideInInspector] public float TimesSum;    //��czny czas wszystkich pomiar�w
    [HideInInspector] public int Precision=4;   //Do ilu miejsc po przecinku zaokr�glamy czas 
    
    /// <summary>
    /// Rozpoczyna pomiar czasu w TimeElapsed
    /// </summary>
    public void StartMeasure()
    {
        TimeElapsed = 0;
        measure = true;
    }
    
    /// <summary>
    /// Zatrzymuje pomiar czasu
    /// </summary>
    public void StopMeasure()
    {
        measure=false;
        TimesSum += TimeElapsed;
        //Put it into database
    }

  
    private void Update()
    {
        if (!measure)
            return;

        TimeElapsed /= 1000;            //z milisekund na sekundy
        TimeElapsed += Time.deltaTime;  //odejmujemy czas w sekundach
        TimeElapsed = (float)Math.Round(TimeElapsed, Precision);    //zaokr�glamy do 4 miejsc po przecinku
        TimeElapsed *= 1000;            //zmieniamy milisekundy z sekund
    }
}
