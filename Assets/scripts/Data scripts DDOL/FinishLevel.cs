using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class FinishLevel : MonoBehaviour
{
    ddol_RoomPlacement data;                
    [HideInInspector]public int id = 0; //Room id

    /// <summary>
    /// Increases finished room to our room
    /// </summary>
    public void Finish()
    {
        if(data == null)
        {
            data = GetComponent<ddol_RoomPlacement>();
        }

       if(data.RoomNumber <= id)
        {
            data.RoomNumber = id+1;
        }
    }
}
