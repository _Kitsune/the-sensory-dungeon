using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMusicVolume : MonoBehaviour
{
    [SerializeField] AudioSource audioPlayer;   //ddol audio source
    private void Start()
    {
        GetComponent<ddol_Data>().MusicVolume = 40;
        GetComponent<ddol_Data>().SoundVolume = 40;
    }
    void Update()
    {
        audioPlayer.volume = (float)((float)GetComponent<ddol_Data>().MusicVolume / 100);
    }
}
