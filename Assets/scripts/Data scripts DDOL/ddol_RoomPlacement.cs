using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ddol_RoomPlacement : MonoBehaviour
{
    [HideInInspector]public List<GenerateDungeonRooms.Room> Rooms;  //Zapis pokoi
    [HideInInspector]public bool GenerateRooms = true;              //Czy ma generowa� ponownie pokoje przy wej�ciu do odpowiedniej sceny
    [HideInInspector]public int RoomNumber = 0;                     //In what room are we
    [HideInInspector]public int DungeonPoints = 0;                  //Overall oone dungeon run score
}
