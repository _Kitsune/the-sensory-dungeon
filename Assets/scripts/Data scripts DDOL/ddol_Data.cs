using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ddol_Data : MonoBehaviour
{
    [HideInInspector] public string UserKey = "12345678ab";               //Klucz u�ytkownika
    [HideInInspector] public int SoundVolume = 20;       //Ustawiona przez niego g�o�no�� d�wi�k�w
    [HideInInspector] public int MusicVolume = 40;       //Ustawiona przez niego g�o�no�� muzyki
    [HideInInspector] public int UserId = -1;           //ID u�ytkownika w bazie   
    [HideInInspector] public int LevelPlayCount = 0;    //How many times a single level was played (room zero - repeat increases it to 2)
     public bool Offline = true;                        //Are ya offline
}
