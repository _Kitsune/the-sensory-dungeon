using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Localization.SmartFormat.Extensions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Playables;
using System;

public class NewUserController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI userName;                //tmpro of input
    [SerializeField] TextMeshProUGUI Output;                  //tmpro with output messages
    dbmanager dbManager;

    public LocalizedStringHandler WrongAnswer;

    private void Start()
    {
        WrongAnswer.Init();
        dbManager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
    }
    /// <summary>
    /// Send user input into database
    /// </summary>
    public void Submit()
    {
        string username = userName.text;
        dbManager.Key = dbmanager.RandomString(10);
        StartCoroutine(dbManager.InsertNewUser(username));
    }

    /// <summary>
    /// Saves to /save.dat given input only when in inspector
    /// </summary>
    /// <param name="toWrite">input to write</param>
    public void SaveFile(string toWrite)
    {
        if (!Application.isEditor)
        {
            return;
        }
            string destination = Application.persistentDataPath + "/save.dat";
        using (StreamWriter sw = File.AppendText(destination))
        {
            sw.WriteLine(toWrite);
        }


    }


    private void Update()
    {
        if(dbManager == null) //if it somehow doesn't exist
        {
            dbManager = GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>();
            return;
        }
        if(dbManager.ReturnValue == "I")    //Incorrect entry, user have put wrong data - already used name or illegal
        {
            Output.text = WrongAnswer.UsageString;
        }else if(dbManager.ReturnValue == "C")  //Correct input - to player prefs and scene change
        {
            
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().UserKey = dbManager.Key;
            PlayerPrefs.SetString("userName", dbManager.CurrentUser.userName);
            PlayerPrefs.SetString("userKey", dbManager.Key);
            PlayerPrefs.SetInt("userId", dbManager.CurrentUser.id);

            SaveFile(dbManager.CurrentUser.userName + " : " + dbManager.Key + Environment.NewLine);

           dbManager.gameObject.GetComponent<SceneChange>().ChangeScene("MainMenu");
            
            
        }
    }

}
