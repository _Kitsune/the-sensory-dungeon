using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoadComponent : MonoBehaviour
{
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("DDOL");  //Bierze wszystkie obiekty z tym tagiem

        if (objs.Length > 1)                                            //Sprawdza czy istnieje
        {
            Destroy(this.gameObject);                                   //Je�li tak to usuwa ten
        }

        DontDestroyOnLoad(this.gameObject);                             //Je�li nie to sprawia by si� nie usuwa� przy zmianie sceny
    }

  
}
