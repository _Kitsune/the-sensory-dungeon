using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class Validator : MonoBehaviour
{
    [SerializeField]TMP_InputField input;

    /// <summary>
    /// Validates input with Polish defined glyph
    /// </summary>
    /// <param name="c">char to validate</param>
    /// <returns>char if correct \0 if not</returns>
    public char Validate(char c)
    {
        const string glyphs = "aA��bBcC��dDeE��fFgGhHiIjJkKlL��mMnN��oO��pPqQrRsS��tTuUvVwWxXyYzZ����0123456789 _-&+";

        if(glyphs.Contains(c))
        {
            return c;
        }

        return '\0';
    }
    private void Start()
    {
        input.onValidateInput += delegate (string input, int charIndex, char charAdded) { return Validate(charAdded); };
    }

}
