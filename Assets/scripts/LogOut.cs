using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogOut : MonoBehaviour
{
   
    public void Logout()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.DeleteKey("userName");
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>().Logged = 0;
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>().ReturnValue = "";
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<dbmanager>().CurrentUser = new dbmanager.User();
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().GenerateRooms = true;
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<SceneChange>().ChangeScene("FirstScene");
    }
}
