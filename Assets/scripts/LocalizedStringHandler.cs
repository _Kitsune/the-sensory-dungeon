using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;


/// <summary>
/// Klasa do obs�ugi LocalizedString, kt�ra aktualizuje ich warto�� kiedy trzeba
/// UsageString jako obiekt do kt�rego odnosimy si� w skryptach
/// W Start() lub Awake() trzeba zrobi� Init(), albo b�dziemy mieli pusty string
/// </summary>
[Serializable]
public class LocalizedStringHandler
{
    public LocalizedString LanguageString = new LocalizedString();          //Localized String do kt�rego si� odnosimy
    [HideInInspector]public string UsageString;                             //String do kt�rego si� odnosimy w innych skryptach

    /// <summary>
    /// Funkcja do zaincjowania tekstu, powinna zosta� odpalona w Start lub Awake
    /// </summary>
    public void Init()
    {
        LanguageString.StringChanged += onChange;
        UsageString = LanguageString.GetLocalizedString();
    }

    /// <summary>
    /// Funkcja pod wydarzenie zmiany j�zyka, lub go ustawienia, od�wie�enia
    /// </summary>
    /// <param name="newString">Nowy tekst</param>
    void onChange(string newString)
    {
        UsageString = newString;
    }

}
