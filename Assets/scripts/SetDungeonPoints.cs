using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetDungeonPoints : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI points;                //Text in which we show points
    [SerializeField] LocalizedStringHandler pointsText;     //What we show with the text
    void Start()
    {
        pointsText.Init();
        int pointsNumber = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().DungeonPoints;
        //reset points
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().DungeonPoints = 0;

        points.SetText(pointsText.UsageString + pointsNumber.ToString());
    }


}
