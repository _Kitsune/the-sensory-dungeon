using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Text;
using System;
using static dbmanager;
using System.Net;
using System.Linq;
using static System.Net.WebRequestMethods;

public class dbmanager : MonoBehaviour
{
    //API links
    string certainUserURL = "http://sdproject.scienceontheweb.net/src/php/user.php?UserName=";                  //get 1 user info
    string addNewUserURL = "http://sdproject.scienceontheweb.net/src/php/AddNewUser.php";                       //add new user with keyPost and userNamePost
    string updateUserPointsURL = "http://sdproject.scienceontheweb.net/src/php/UpdateUser.php";                 //Update user with id and new point value  (insertive not additive)
    string certainUserKeyURL = "http://sdproject.scienceontheweb.net/src/php/UserSettings.php?UserName=";       //get 1 user key from username, key is already hashed on the site, so we compare hashes

    //Fields to update or insert data
    string postUsernameName = "users";          //Post field for user name
    string postUsernameID = "id";               //Post field for id
    string postKey = "key";                     //Post field for key
    string postUserPoints = "usersPoints";      //Post field for userPoints

    public string ReturnValue = "-1";           //Correctly created a User - I = Incorrect. C = Correct

    public int Logged = 0;                      //1 yes, -1 no

    public User CurrentUser;                                //As the name suggests
    public string Key = "12ds56gh90";                       //UserKey


    /// <summary>
    /// Hashes given string
    /// </summary>
    /// <param name="s">string to hash</param>
    /// <returns>Hash of length 10</returns>
    public static string Hash(string s)
    {
        string a = "";
        for(int j =0;j<10;j++)
        {
            int b = 0;
            for (int i = 0; i < s.Length; i++)
            {
                b += (int)(s[i]) * (j + i); 
            }
            a += (char)((b%96) + 30);
        }
        
        return a;
    }

    /// <summary>
    /// Generates Random string consisting of normal alphabet and numbers
    /// </summary>
    /// <param name="length">Length of returned string</param>
    /// <returns>Random string</returns>
    public static string RandomString(int length)
    {
        string temp = "";
        const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (int i = 0; i < length; i++)
        {
            temp += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
        }

        return temp;
    }

    /// <summary>
    /// Class to Neglect bad SSLs, we trust those sites
    /// </summary>
    public class ForceAcceptAll : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }
    
    public class User
    {
        public int id { get; set; }
        public string userName { get; set; }
        public int allPoints { get; set; }
    }


    public int Finished = -2;
    // -2 = not even started
    // -1 = oo an error
    //  0 = running
    //  1 = finished correctly

    private void Awake()
    {
        CurrentUser = new User();
    }

    void Update()
    {
        if (Finished == -2)
            return;

        //If we selected the data and it works
        if (Finished == 1)
        {
            Finished = -2;
            GetComponent<ddol_Data>().UserId = CurrentUser.id;
            GetComponent<ddol_Data>().UserKey = Key;
            GetComponent<SceneChange>().ChangeScene("MainMenu");
        }

    }


    /// <summary>
    /// Function for outside to get user info from db and put it into CurrentUSer
    /// </summary>
    /// <param name="userName">user name</param>
    public void Select(string userName)
    {
        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline) //We can't get info if we are offline
        {
            return;
        }

        string url = certainUserURL + userName;
        CurrentUser.userName = userName;
        Finished = 0;
        StartCoroutine(Get(url));

    }
    
    /// <summary>
    /// Tries to login, sets most of the variables after login, also into player prefs
    /// </summary>
    /// <param name="userName">UserName</param>
    /// <param name="key">Key</param>
    /// <returns></returns>
    public IEnumerator TryToLogin(string userName, string key)
    {
        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline)
        {
            yield return 1;
        }
        else
        {
            userName = userName.Substring(0, userName.Length - 1);
            key = key.Substring(0, key.Length - 1);

            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true; //We do not care about SSLs here, everything is valid

            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString(certainUserKeyURL + userName); //site content
                if(s.Length == 2)
                {
                    Logged = -1;
                }else
                {
                   
                    string gotKey = s.Substring(2, 10);
                    if (gotKey == dbmanager.Hash(key))
                    {
                        Select(userName);
                        Logged = 1;
                        PlayerPrefs.SetString("userName", userName);
                        PlayerPrefs.SetString("userKey", key);
                        PlayerPrefs.SetInt("userId", CurrentUser.id);
                    }
                    else
                    {
                        Logged = -1;
                    }
                }

            }
            yield return true;
        }


    }

    /// <summary>
    /// Gets user info from url api, and puts it into CurrentUser
    /// </summary>
    /// <param name="url">api for single user url</param>
    /// <returns></returns>
    public IEnumerator Get(string url)
    {
        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline)
        {
            //default values for offline mode
            CurrentUser.allPoints = 0;
            CurrentUser.id = 0;
            Key = "0";
            yield return true;
        }
        else
        {
            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true; //We do not care about SSLs here, everything is valid

            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString(url); //site content

                Finished = 1;
                string[] tokens = s.Split(',');
                //0 - id
                //1 - username
                //2 - points
                    
                CurrentUser.id = int.Parse(tokens[0]);
                CurrentUser.userName = tokens[1];
                CurrentUser.allPoints = int.Parse(tokens[2]);

            }
            yield return true;
        }

    }
    
    
    /// <summary>
    /// Adds Current user points
    /// </summary>
    /// <param name="points">How many points to add</param>
    /// <returns></returns>
    public IEnumerator UpdatePoints(int points)
    {
        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().DungeonPoints += points;
        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline)
        {
            yield return 1;
        }
        else
        {
            CurrentUser.allPoints += points;

            string url = updateUserPointsURL;
            WWWForm form = new WWWForm();
            form.AddField(postUsernameID, CurrentUser.id);
            form.AddField(postUserPoints, CurrentUser.allPoints);
            using (UnityWebRequest www = UnityWebRequest.Post(url, form))
            {
                www.certificateHandler = new ForceAcceptAll();  //cause it may not won't top get from sources with bad SSLs

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError || www.downloadHandler.text.Contains("Uncaught mysqli_sql_exception:"))
                {
                    Debug.Log(www.error);
                }
                else
                {

                }
            }

            yield return true;
        }
    }


    /// <summary>
    /// Tries to insert user into db
    /// </summary>
    /// <param name="username">With that username</param>
    /// <returns>ReturnValue as I - for wrong, and C - for correct</returns>
    public IEnumerator InsertNewUser(string username)
    {
        if (GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().Offline)
        {
            yield return 1;
        }
        else
        {
            string url = addNewUserURL;
            WWWForm form = new WWWForm();

            username = username.Substring(0, username.Length - 1);

            form.AddField(postUsernameName, username);
            form.AddField(postKey,dbmanager.Hash(Key));
            
            using (UnityWebRequest www = UnityWebRequest.Post(url, form))
            {
                www.certificateHandler = new ForceAcceptAll();  //cause it may not won't top get from sources with bad SSLs

                yield return www.SendWebRequest();
                if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError ||www.downloadHandler.text.Contains("Uncaught mysqli_sql_exception:"))
                {
                    Debug.Log(www.error);
                    
                    ReturnValue = "I";          //Something went wrong is what we say
                }
                else
                {
                    CurrentUser.userName = username;
                    Select(username);//To get User ID into object

                    ReturnValue = "C";
                }
            }
           
        }   
            
       

    }
}
