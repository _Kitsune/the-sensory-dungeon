using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishDungeon : MonoBehaviour
{
    void Start()
    {
        int RoomId = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().RoomNumber;
        if (RoomId > GenerateDungeonRooms.RoomCount)    //have we finished all of the rooms in this dungeon
        {
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>().GenerateRooms = true;
            
            GameObject.FindGameObjectWithTag("DDOL").GetComponent<SceneChange>().ChangeScene("FinishDungeon");
        }
    }
}
