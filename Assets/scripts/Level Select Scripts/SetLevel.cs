using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLevel : MonoBehaviour
{
    [HideInInspector] public int id;
    
    /// <summary>
    /// Changes state of the level we are in, to finish it later
    /// </summary>
    public void ChangeLevel()
    {
        if (GameObject.FindGameObjectsWithTag("DDOL").Length <= 0)
            return;

        GameObject.FindGameObjectWithTag("DDOL").GetComponent<FinishLevel>().id = id;
    }
}
