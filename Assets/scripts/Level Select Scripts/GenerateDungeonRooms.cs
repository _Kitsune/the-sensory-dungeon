using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class GenerateDungeonRooms : MonoBehaviour
{
    [Serializable]public enum RoomType
    {
        visual,
        sound,
        visual_sound,
        spatial,
        accuracy,
        sequence,
        LAST
    }
    [Serializable]public struct TypeSceneMap
    {
        public RoomType Type;
        public string SceneName;
        public GameObject Myself;
    }

    [SerializeField]TypeSceneMap[] typeSceneMap;

    [SerializeField]GameObject[] Rooms;         //Obiekty pokoi, te z do�u
    [SerializeField]Transform StartingPoint;    //Od jakiego miejsca zaczyna si� generacji
    [SerializeField]Transform Parent;           //Jaki obiekt b�dzie rodzicem stworzonych obiekt�w - MUSI by� to obiekt pod CANVASEM
    
    System.Random random;                       //Systemowa zmienna od losowo�ci
    public const int RoomCount = 10;            //Ile pokoi generujemy
    public int DungeonNumber = 0;               //Numer Dungeonu potrzebny do poprawnej zmiany sceny
    public bool ReGenerate = true;              //Czy generujemy poziomy znowu
    [Serializable]public struct Room            //Typ pokoju (Pozycja Typ)
    {
        public Vector2 Position;
        public TypeSceneMap Type;
        public string SceneName;
    }          
    Room[] rooms;                               //Wygenerowane pokoje
    int yOffset = 0;                            //Jak nisko/wysoko doszli�my
    
    /// <summary>
    /// Generuje obiekty z tablicy rooms
    /// </summary>
    void GenerateRoomObjects()
    {
        ddol_RoomPlacement roomData = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>();
        int i = 0;
        foreach (Room room in rooms)
        {
            GameObject BaseObjectToInstantiate = Rooms[random.Next(0, Rooms.Length - 1)];           //Element dolny bazy


            GameObject TypeObjectToInstantiate = room.Type.Myself;                                  //Element g�rny typu

            Vector3 ObjectPosition = StartingPoint.position;                                        //Ustawiamy startow� pozycje
            ObjectPosition.x += (room.Position.x * 1.2f);                                                    //Dodajemy odpowiedni x patrz�c na pozycje gridow�
            ObjectPosition.y += (room.Position.y * 1.5f);                                                    //Dodajemy odpowiedni y patrz�c na pozycje gridow�

            GameObject Base = Instantiate(BaseObjectToInstantiate, ObjectPosition, Quaternion.identity, Parent);    //Tworzymy baz�
            
            //Base.AddComponent<Button>();                                                                            //Dodajemy bazie guzik
            //Base.GetComponent<Button>().transition = Selectable.Transition.None;                                    //Sprawiamy by nie mia� animacji przejs�ia

            Base.AddComponent<SetLevel>();
            Base.GetComponent<SetLevel>().id = i + 1;
            Base.GetComponent<Button>().onClick.AddListener(Base.GetComponent<SetLevel>().ChangeLevel);

            //Dodajemy mu zmian� sceny na odpowiedni�
            Base.GetComponent<Button>().onClick.AddListener(delegate { Base.GetComponent<SceneChange>().ChangeScene(room.SceneName); });
            Base.transform.localScale = Vector3.one * 80;        //Skalujemy baz� by mia�a widoczny rozmiar



           
            if(roomData.RoomNumber <= i)
            {
                Base.GetComponent<Button>().interactable = false;
            }
            else
            {
                GameObject onTop = Instantiate(TypeObjectToInstantiate, ObjectPosition, Quaternion.identity, Base.transform);              //Tworzymy typ

                onTop.GetComponent<SpriteRenderer>().sortingOrder = 1;
                onTop.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            }

            i++;
        }
        
    }

    /// <summary>
    /// Nadaje pokojowi o podanym id losowy typ
    /// </summary>
    /// <param name="i">ID pokoju w tablicy</param>
    void GiveRoomAType(int i)
    {
        rooms[i].Type = typeSceneMap[UnityEngine.Random.Range(0, (int)RoomType.LAST)];
    }
    
    /// <summary>
    /// Sprawdza czy mo�na po�o�y� pok�j na podanych koordynatach, czy nie nachodzi wtedy na inny
    /// </summary>
    /// <param name="x">Pozycja X gridowa</param>
    /// <param name="y">Pozycja Y gridowa</param>
    /// <returns>Czy mo�na po�o�y� tam pok�j (True = mo�na)</returns>
    bool Place(int x, int y)
    {
        foreach (Room r in rooms)
        {
            if(r.Position.x == x && r.Position.y == y)
            {
                return false;
            }
        }
        return true;
    }
   
    /// <summary>
    /// Nadajemy pozycje pokojowi o id podanym
    /// </summary>
    /// <param name="i"></param>
    void SetPositions(int i)
    {
        //Je�li to pierwszy element to dajemy go na �rodek
        if(i==0)
        {
            rooms[i].Position = new Vector2(0, 0);
            return;
        }

        //Dop�ki nie znajdziemy losujemy jeden z s�siednich
        bool Run = true;
        while(Run)
        {
            int temp_YOffset = yOffset;             //ustawiamy obecny offset
            Vector2 Next = rooms[i - 1].Position;   //bierzemy poycje poprzedniego pokoju
            int Change = random.Next(1, 6);         //patrzymy w kt�r� stron� (lewo prawo po 2, g�ra d� po 1)
            switch(Change)
            {
                case 1:
                case 5:
                    Next.x += 1;
                    break;
                case 2:
                case 6:
                    Next.x -= 1;
                    break;
                case 3:
                    Next.y += 1;
                    temp_YOffset++;
                    break;
                case 4:
                    Next.y -= 1;
                    temp_YOffset--;
                    break;
            }

            if(Math.Abs(temp_YOffset) < 3 && Place((int)Next.x, (int)Next.y))   //Sprawdzamy czy nie wychodzimy za limit Y, je�li nie to wychodzimy z p�tli
            {
                yOffset = temp_YOffset;
                rooms[i].Position = Next;
                Run = false;
            }
            
        }
    }
    
    /// <summary>
    /// Tworzy pokoje
    /// </summary>
    void InitNodes()
    {
        for (int i = 0; i < RoomCount; i++)
        {
            rooms[i] = new Room();
            SetPositions(i);
            GiveRoomAType(i);
            GiveRoomACorrectSceneName(i);
        }
    }
    
    /// <summary>
    /// Gives a room correct scene Name
    /// </summary>
    /// <param name="i">room index</param>
    void GiveRoomACorrectSceneName(int i)
    {
        foreach(TypeSceneMap tsm in typeSceneMap)
        {
            if(tsm.Type == rooms[i].Type.Type)
            {
                rooms[i].SceneName = tsm.SceneName;
                break;
            }
        }
    }


    void Start()
    {
        //Sprawdzamy czy generowali�my wcze�niej pokoje i czy mamy je generowa� kolejny raz
        
        random = new System.Random();
        rooms = new Room[RoomCount];

        GameObject ddol = GameObject.FindGameObjectWithTag("DDOL");
        if (ddol != null) //Je�li pokoje by�y wygenerowane
        {
            ReGenerate = ddol.GetComponent<ddol_RoomPlacement>().GenerateRooms;
            if (ReGenerate == false)    //By�y wygenerowane, maj� zosta� tamte
            {
                rooms = ddol.GetComponent<ddol_RoomPlacement>().Rooms.ToArray();    //bierzemy pokoje
                GenerateRoomObjects();                                              
                return;
            }else                                                                   //S� ale mamy wygenerowa� nowe
            {
                InitNodes();
                ddol_RoomPlacement roomData = GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_RoomPlacement>();
                roomData.RoomNumber = 1;

                GenerateRoomObjects();
                ddol.GetComponent<ddol_RoomPlacement>().Rooms.Clear();
                ddol.GetComponent<ddol_RoomPlacement>().Rooms.AddRange(rooms);
                ddol.GetComponent<ddol_RoomPlacement>().GenerateRooms = false;
                
                return;
            }
        }

        InitNodes();
        GenerateRoomObjects();


    }
}
