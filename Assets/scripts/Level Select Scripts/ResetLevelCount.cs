using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLevelCount : MonoBehaviour
{
    /// <summary>
    /// Resets how many times a single level was played
    /// </summary>
    public void Reset()
    {
        if(GameObject.FindGameObjectsWithTag("DDOL").Length<=0)
            return;

        GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().LevelPlayCount = 0;
    }
}
