using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets audiosource of object it is on to correct volume - sound Volume
/// </summary>
public class SetSourceSound : MonoBehaviour
{

    void Update()
    {
        AudioSource audioPlayer = GetComponent<AudioSource>();
        audioPlayer.volume = (float)((float)GameObject.FindGameObjectWithTag("DDOL").GetComponent<ddol_Data>().SoundVolume / 100);
    }
}
